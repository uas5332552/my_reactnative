import {StyleSheet} from 'react-native';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import {Beranda, Notifikasi, ProdukBaru, Setelan, Transaksi, pesan} from '.';

const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

function MenuBawah() {
  return (
    <Tab.Navigator
      initialRouteName="Feed"
      activeColor="#e91e63"
      barStyle={{backgroundColor: '#041fc5'}}>
      <Tab.Screen
        name="Beranda"
        component={Beranda}
        options={{
          headerTintColor: '#041fc5',

          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="home" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Produk Baru"
        component={ProdukBaru}
        options={{
          headerTintColor: '#041fc5',

          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons
              name="bag-checked"
              color={color}
              size={size}
            />
          ),
        }}
      />

      <Tab.Screen
        name="Transaksi"
        component={Transaksi}
        options={{
          headerTintColor: '#041fc5',

          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="receipt" color={color} size={size} />
          ),
        }}
      />

      <Tab.Screen
        name="Notifikasi"
        component={Notifikasi}
        options={{
          headerTintColor: '#041fc5',

          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="bell" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Setelan"
        component={Setelan}
        headerShadow={false}
        options={{
          headerTintColor: '#041fc5',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="cog" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
function ScreenOpsi() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Pesan" component={pesan} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: '#041fc5',
            alignItems: 'center',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontweight: 'bold',
          },
        }}>
        <Stack.Screen name="BOOKWORMS" component={MenuBawah} />
        <Stack.Screen name="Pesan" component={ScreenOpsi} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;

const styles = StyleSheet.create({});
