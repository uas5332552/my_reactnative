import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';

const IconMenu = ({label, assets}) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.icon}>
        <Image source={assets} style={styles.img}></Image>
        <Text style={styles.txt}>{label}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default IconMenu;

const styles = StyleSheet.create({
  img: {
    width: 50,
    height: 50,
    justifyContent: 'center',
    margin: '0 auto',
  },
  icon: {
    margin: 10,
    padding: 5,
    borderWidth: 2,
    borderColor: 'blue',
    backgroundColor: 'white',
    width: 100,
    height: 90,
    alignItems: 'center',
    borderRadius: 15,
  },
  txt: {
    color: 'blue',
    padding: 6,
  },
});
