import Beranda from './screens/beranda';
import Setelan from './screens/setelan';
import Notifikasi from './screens/notifikasi';
import Transaksi from './screens/transaksi';
import ProdukBaru from './screens/produkbaru';
import pesan from './opsiscreen/pesan';

export {Beranda, Setelan, Notifikasi, Transaksi, ProdukBaru, pesan};
