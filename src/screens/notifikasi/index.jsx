import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import React from 'react';

const Notifikasi = ({navigasi}) => {
  return (
    <View>
      <TouchableOpacity onPress={() => navigasi.navigate('Pesan')}>
        <View style={styles.kotak}>
          <View>
            <Image
              source={require('../../assets/icon/chat.png')}
              style={styles.img}
            />
          </View>

          <View style={styles.text}>
            <Text style={styles.textkotak}>Pesan</Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default Notifikasi;

const styles = StyleSheet.create({
  kotak: {
    flexDirection: 'row',
    backgroundColor: '#D5D9F6',
    height: 60,
    margin: 4,
  },
  img: {
    width: 50,
    height: 50,
    paddingTop: 4,
    margin: 4,
  },
  text: {
    justifyContent: 'center',
    backgroundColor: 'white',
    width: 300,
  },
  textkotak: {
    padding: 10,
    fontSize: 20,
    color: '#041fc5',
  },
});
