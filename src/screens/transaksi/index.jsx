import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import React from 'react';

const Transaksi = () => {
  return (
    <View>
      <TouchableOpacity>
        <View style={styles.kotak}>
          <View>
            <Image
              source={require('../../assets/img/1.jpeg')}
              style={styles.img}></Image>
          </View>
          <View style={styles.text}>
            <Text style={styles.textkotak}>
              Transaksi Pembelian Buku "Saatnya Muda"
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default Transaksi;

const styles = StyleSheet.create({
  kotak: {
    flexDirection: 'row',
    backgroundColor: '#D5D9F6',

    height: 80,
    margin: 4,
  },
  img: {
    width: 70,
    height: 70,
    paddingTop: 4,
    margin: 4,
  },
  text: {
    justifyContent: 'center',
    backgroundColor: 'white',
    padding: 5,
  },
  textkotak: {
    textAlign: 'center',

    fontSize: 16,
    color: 'black',
  },
});
