import {StyleSheet, Text, View, ScrollView} from 'react-native';
import React from 'react';

const ProdukBaru = () => {
  return (
    <View>
      <ScrollView>
        <View style={styles.produkbaru}>
          <View style={styles.kotakawal}>
            <View style={styles.kotak}>
              <Text style={styles.textkotak}>Tidak Ada Produk</Text>
            </View>
            <View style={styles.kotak}>
              <Text style={styles.textkotak}>Tidak Ada Produk</Text>
            </View>
          </View>
          <View style={styles.kotakkedua}>
            <View style={styles.kotak}>
              <Text style={styles.textkotak}>Tidak Ada Produk</Text>
            </View>
            <View style={styles.kotak}>
              <Text style={styles.textkotak}>Tidak Ada Produk</Text>
            </View>
          </View>
          <View style={styles.kotaktiga}>
            <View style={styles.kotak}>
              <Text style={styles.textkotak}>Tidak Ada Produk</Text>
            </View>
            <View style={styles.kotak}>
              <Text style={styles.textkotak}>Tidak Ada Produk</Text>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default ProdukBaru;

const styles = StyleSheet.create({
  produkbaru: {
    padding: 5,
    margin: 5,
    borderTopColor: '#041fc5',
    borderTopWidth: 2,
  },
  Text: {
    color: '#041fc5',
    textAlign: 'center',
    fontSize: 20,
  },
  kotakawal: {
    justifyContent: 'space-around',
    flexDirection: 'row',
  },
  kotakkedua: {
    justifyContent: 'space-around',
    flexDirection: 'row',
  },
  kotaktiga: {
    justifyContent: 'space-around',
    flexDirection: 'row',
  },
  kotak: {
    backgroundColor: '#D5D9F6',
    width: 150,
    height: 170,
    padding: 10,
    margin: 10,
  },
  textkotak: {
    textAlign: 'center',
  },
});
