import {
  StyleSheet,
  View,
  FlatList,
  ScrollView,
  RefreshControl,
} from 'react-native';
import React, {useState} from 'react';
import IconMenu from '../../components/IconMenu';
import {Text} from 'react-native-paper';

const Data = [
  {id: 0, label: 'COD', assets: require('../../assets/icon/1.jpg')},
  {id: 1, label: 'Gratis Ongkir', assets: require('../../assets/icon/2.png')},
  {id: 2, label: 'Flash Sale', assets: require('../../assets/icon/4.png')},
  {id: 3, label: 'Promo', assets: require('../../assets/icon/3.jpeg')},
];

const Beranda = () => {
  const [refreshing, setRefreshing] = useState(false);
  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 1000);
  };
  return (
    <View>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        <View style={{backgroundColor: '#D5D9F6'}}>
          <FlatList
            data={Data}
            renderItem={({item}) => (
              <IconMenu label={item.label} assets={item.assets} />
            )}
            horizontal={true}
          />
        </View>
        <View style={styles.rekome}>
          <Text style={styles.Text}>Rekomendasi</Text>
          <View style={styles.kotakawal}>
            <View style={styles.kotak}>
              <Text style={styles.textkotak}>Tidak Ada Produk</Text>
            </View>
            <View style={styles.kotak}>
              <Text style={styles.textkotak}>Tidak Ada Produk</Text>
            </View>
          </View>
          <View style={styles.kotakkedua}>
            <View style={styles.kotak}>
              <Text style={styles.textkotak}>Tidak Ada Produk</Text>
            </View>
            <View style={styles.kotak}>
              <Text style={styles.textkotak}>Tidak Ada Produk</Text>
            </View>
          </View>
          <View style={styles.kotaktiga}>
            <View style={styles.kotak}>
              <Text style={styles.textkotak}>Tidak Ada Produk</Text>
            </View>
            <View style={styles.kotak}>
              <Text style={styles.textkotak}>Tidak Ada Produk</Text>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default Beranda;

const styles = StyleSheet.create({
  rekome: {
    padding: 5,
    margin: 5,
    borderTopColor: '#041fc5',
    borderTopWidth: 2,
  },
  Text: {
    color: '#041fc5',
    textAlign: 'center',
    fontSize: 20,
  },
  kotakawal: {
    justifyContent: 'space-around',
    flexDirection: 'row',
  },
  kotakkedua: {
    justifyContent: 'space-around',
    flexDirection: 'row',
  },
  kotaktiga: {
    justifyContent: 'space-around',
    flexDirection: 'row',
  },
  kotak: {
    backgroundColor: '#D5D9F6',
    width: 150,
    height: 170,
    padding: 10,
    margin: 10,
  },
  textkotak: {
    textAlign: 'center',
  },
});
